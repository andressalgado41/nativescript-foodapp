import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as localStorage from 'nativescript-localstorage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient) { }

  public login( email: string, password: string ) {

    return this.http.post('https://reqres.in/api/login',JSON.stringify({
      email: email,
      password: password
    }), {
      headers: this.commonHeaders()
    });
  }

  public register( email: string, password: string ) {

    return this.http.post('https://reqres.in/api/register',JSON.stringify({
      email: email,
      password: password
    }), {
      headers: this.commonHeaders()
    });

  }

  private commonHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  public setUser(type: string, data:any, email:string, name: string ) {
    localStorage.setItem('user', JSON.stringify({
      type,
      data,
      user: {
        email: email,
        name: name
      }
    }));
  }

  public currentUser() {
    return  JSON.parse(localStorage.getItem('user')) || null;
  }

  public deleteUser() {
    localStorage.removeItem('user');
  }

}
