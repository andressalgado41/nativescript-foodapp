import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { AuthService } from '../services/auth.service';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public email: string = 'eve.holt@reqres.in';
  public password: string = 'cityslicka';

  constructor(private page: Page, private authService:AuthService, private router: RouterExtensions) { 
    this.page.actionBarHidden = true;
  }

  ngOnInit() {
  }

  public onLogin() {
  
    this.authService.login( this.email, this.password ).subscribe( res => {
      this.authService.setUser( 'food-app', res, this.email, this.password );
      this.router.navigate( [ '/home/menu' ], { transition: { name: 'slide' }, clearHistory: true } )
    } );
  
  }

}
