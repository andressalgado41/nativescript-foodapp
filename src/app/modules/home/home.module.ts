import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { RestaurantsComponent } from './restaurants/restaurants.component';

import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular/listview-directives"; 
import { NativeScriptMaterialCardViewModule } from "nativescript-material-cardview/angular";
import { registerElement } from "nativescript-angular/element-registry";
registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);
import { RestaurantDetailComponent } from './restaurant-detail/restaurant-detail.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
  {path:'menu',component: BottomBarComponent},
  {path:'restaurant-detail',component: RestaurantDetailComponent}
]

@NgModule({
  declarations: [
    BottomBarComponent, 
    RestaurantsComponent, 
    RestaurantDetailComponent, 
    ProfileComponent
  ],
  imports: [
    CommonModule,
    NativeScriptUIListViewModule,
    NativeScriptMaterialCardViewModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forChild(routes)
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HomeModule { }
