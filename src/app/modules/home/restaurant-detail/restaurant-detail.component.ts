import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { MapboxViewApi} from "nativescript-mapbox";
import { RouterExtensions } from 'nativescript-angular/router';


@Component({
  selector: 'ns-restaurant-detail',
  templateUrl: './restaurant-detail.component.html'
})
export class RestaurantDetailComponent implements OnInit {

  public restaurant: any;
  private map: MapboxViewApi;

  constructor(private page: Page, private params: ActivatedRoute, private router: RouterExtensions) {
    page.actionBarHidden = true;
   }

  ngOnInit() {
    this.restaurant = JSON.parse(this.params.snapshot.queryParams['restaurant']).restaurant;
  }

  onMapReady(args, restaurant): void {
    this.map = args.map;
    this.map.addMarkers([
      {
        lat: restaurant.location.latitude,
        lng: restaurant.location.longitude,
        title: restaurant.name,
        subtitle: restaurant.timings
      }
    ]);
  }

  onBack(){
    this.router.back();
  }

}
