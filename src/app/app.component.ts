import { Component, AfterViewInit } from "@angular/core";
import * as firebase from 'nativescript-plugin-firebase';
import { Message } from "nativescript-plugin-firebase";

@Component({
    selector: "ns-app",
    templateUrl: "./app.component.html"
})
export class AppComponent implements AfterViewInit {

    ngAfterViewInit() {
        firebase.init({
            iOSEmulatorFlush: true
            // onMessageReceivedCallback: (message: Message) => {
            //     console.log(`Title: ${message.title}`);
            //     console.log(`Body: ${message.body}`);
            //     // if your server passed a custom property called 'foo', then do this:
            //     console.log(`Value of 'foo': ${message.data.foo}`);
            //   },
            //   onPushTokenReceivedCallback: (token) => {
            //     console.log("Firebase push token: " + token);
            //   }
        });
    }

 }
